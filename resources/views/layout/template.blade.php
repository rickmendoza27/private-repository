<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>BookStore</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
      <!--   <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css"> -->
                <link rel="stylesheet" href="{{ URL::asset('css/datatables-bootstrap.css') }}">

</head>
<body>
  <main>
    <div class="container">
        @if(Session::has('flash_message'))
            <div class="alert alert-success">
                {{ Session::get('flash_message') }}
            </div>
        @endif
        
        @yield('content')
    </div>
</main>

        <!-- jQuery -->
        <script src="//code.jquery.com/jquery.js"></script>

        <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

        <script src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>

        <script src="{{ URL::asset('js/datatables.fnReloadAjax.js') }}"></script>
        <!-- DataTables -->
        <script src="{{ URL::asset('js/datatables-bootstrap.js') }}"></script>

   

        <!-- DataTables -->
        <!-- Bootstrap JavaScript -->
        



         
        <!-- App scripts -->	
         @stack('scripts')

</body>
</html>