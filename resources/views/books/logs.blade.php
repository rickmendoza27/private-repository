@extends('layout/template')
@section('content')

<h1>User Activity Logs</h1>

<hr>

    <table class="table table-bordered" id="logs-table">
        <thead>
            <tr>
                <th>USERNAME</th>           
                <th>TIME EXECUTED</th>
                <th>CURRENT URL</th>
                <th>IP ADDRESS</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@stop

@push('scripts')
 <script type="text/javascript" >
        $(document).ready(function() {
            $('#logs-table').dataTable( {
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ Records Per Page"
                },
                "bProcessing": true,
                "sAjaxSource": "{{ URL::to('logsdatatables') }}",  
                "aoColumns": [
                    {"mData": "username"},
                    {"mData": "created_at"},
                    {"mData": "current_url"},
                    {"mData": "ip_address"},
                ],
            });
        });

 
    </script>


@endpush
    
@stop