@extends('layout/template')

@section('content')

<h1>Hi ! {{ $users->name }} . Welcome to Mendoza's Book Store <a href="{{ URL::to('logout') }}" class="btn btn-danger">Logout</a>
<a href="{{ URL::to('books/logs') }}" class="btn btn-primary">Show Activity Logs</a></h1>
<a href="{{url('/books/create')}}" class="btn btn-success">Add Book</a>
<br/><br/>
 
<form id="delete-all-form" method="POST" action="{{{ URL::to('books/deleteall') }}}">
    <input type='hidden' name='_token' value='{{{ csrf_token() }}}' />
    <input type='submit' value="Delete All" class="btn-danger">
</form>
<form id="multiple-delete-form" method="POST" action="{{{ URL::to('books/bulkdelete') }}}">
    <input type="hidden" name="multiple_id" id="multiple_id" />
    <input type='hidden' name='_token' value='{{{ csrf_token() }}}' />
    <input class="btn-primary" type="submit" id="deleteBook" value="Delete" />
</form>

<hr>

<table class="table table-bordered" id="books-table">
        <thead>
            <tr>
                <th><input type="checkbox" id="ckbCheckAll" /></th>
                <th>ID</th>           
                <th>ISBN</th>
                <th>TITLE</th>
                <th>AUTHOR</th>
                <th>PUBLISHER</th>
                <th>ACTION</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@stop

@push('scripts')
 <script type="text/javascript" >
        $(document).ready(function() {
            $("#ckbCheckAll").click(function () {
            $(".checkBoxClass").prop('checked', $(this).prop('checked'));
            });
            $("#deleteBook").on("click", function() {
            var checkBox = $(".checkBoxClass:checked");
            var a = new Array();
            $.each(checkBox , function(){
                a.push($(this).val());
            });
            $('#multiple_id').val(a.toString()); //store array
                $('#multiple-delete-form').submit();
            });

            $('#books-table').dataTable( {
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ Records Per Page"
                },
                "bProcessing": true,
                "sAjaxSource": "{{ URL::to('datatables') }}",  
                "aoColumns": [
                    {"mData": null, "mRender": function(data, type, row) {
                    return "<input type='checkbox' class='checkBoxClass' id='checkBoxClass' value='"+ row.id+"'/> ";
                    }},
                    {"mData": "id"},
                    {"mData": "isbn"},
                    {"mData": "title"},
                    {"mData": "author"},
                    {"mData": "publisher"},
                    {"mData": null, "mRender": function(data, type, row) {
                        var actions = "";
                        actions += "<a href=\"{{{ URL::to('books') }}}/" + row.id + "/edit"+ "\" class=\"btn btn-warning\">EDIT</a>";
                        actions += "<form method='POST' action='{{{ URL::to('books') }}}/"+ row.id +"'><input type='hidden' name='_token' value='{{{ csrf_token() }}}' /><input type='submit' class='btn btn-primary' value='DELETE'/></form>"
                        return actions;
                    }},
                ],
            });
        });

 
    </script>


@endpush

@endsection