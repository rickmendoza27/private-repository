@extends('layout.template')
@section('content')

	{!! Form::open(array('method' => 'POST','url' => 'login')) !!}
	<h1>Login</h1>
    <div class="form-group">
	{!! Form::text('email','',array('id'=>'','class'=>'form-control span6','placeholder' => 'Please Enter your Email')) !!}
	</div>
    <div class="form-group">
	{!! Form::password('password',array('class'=>'form-control span6', 'placeholder' => 'Please Enter your Password')) !!}
	</div>
	<p >{!! Form::submit('Login', array('class'=>'btn btn-primary')) !!}</p>
	{!! Form::close() !!}
	 @include('alerts.errors')
 <a href="{{url('/users/create')}}">Don't have account yet? Register Now!</a>
@stop