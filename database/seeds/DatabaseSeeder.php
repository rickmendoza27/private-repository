<?php

// use DB;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
    
        DB::table('users')->insert(array(
            'name'     => 'Rick Mendoza',
            'username' => 'Rick',
            'email'    => 'rickmendoza@yahoo.com',
            'password' => Hash::make('123456'),
        ));
    }
}