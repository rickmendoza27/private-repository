<?php

namespace App\Http\Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Closure;

class LogMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
  public function handle(Request $request, Closure $next)
    {
        
        //logging goes here
        //<space><route><space><array/object>
        
        Log::info($request->path().' '.print_r($request->all(),true));
        return $next($request);
    }

}