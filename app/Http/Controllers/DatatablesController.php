<?php

namespace App\Http\Controllers;
use App\Models\Book;
use App\Models\Logs;
use Response;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DatatablesController extends Controller
{
 
    public function anyData()
    {
        return self::dataTables(Book::select('id','isbn','title','author','publisher')->get());
    }

   public static function dataTables($data) {

    $query = array(
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data);
        return response()->json($query,200);
    }
}
