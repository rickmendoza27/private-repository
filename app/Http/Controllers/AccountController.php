<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Redirect;
use Input;
use Validator;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;

class AccountController extends Controller
{
     public function login() {
    // Getting all post data
    $data = Input::all();
    // Applying validation rules.
    $rules = array(
        'email' => 'required|email',
        'password' => 'required|min:6',
         );
    $validator = Validator::make($data, $rules);
    if ($validator->fails()){
      // If validation falis redirect back to login.
      return Redirect::to('/')->withInput(Input::except('password'))->withErrors($validator);
    }
    else {
      $userdata = array(
            'email' => Input::get('email'),
            'password' => Input::get('password')
          );
      
      // doing login.
      //if (Auth::validate($userdata)) {
        if (Auth::attempt($userdata)) {
          return Redirect::intended('books');
        }
      //} 
      else {
        // if any error send back with message.
        Session::flash('error', 'Invalid Email or Password'); 
        return Redirect::to('/');
      }
    }
  }

  public function logout() {
    Auth::logout(); // logout user
  return Redirect::to('/'); //redirect back to login
}
}
