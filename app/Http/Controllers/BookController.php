<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\User;
use App\Models\Log;
use Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
     public function index()
    {
      //
        $books=Book::all();
        $users= Auth::user();

        return view('books.index',compact('books','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
   return view('books.create');
    }

/**
 * Store a newly created resource in storage.
 *
 * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        'isbn' => 'required',
        'title' => 'required',
        'author' => 'required',
        'publisher' => 'required',
        'image' => 'required'
        ]);

       $book = $request->all();
       Book::create($book);
       Session::flash('flash_message', 'Books successfully added!');

        $activity_log_data = $this->userData($request);
        $activity_log_data['action'] = 'create book: ';
        
        Log::create($activity_log_data);


       return redirect('books');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
    
    $book=Book::find($id);

    return view('books.show',compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
       $book=Book::find($id);

       return view('books.edit',compact('book'));
    }

/**
 * Update the specified resource in storage.
 *
 * @param  int  $id
 * @return Response, 
 */
    public function update($id , Request $request)
    {
       //
        $book = Book::find($id);

        $this->validate($request, [
        'isbn' => 'required',
        'title' => 'required',
        'author' => 'required',
        'publisher' => 'required',
        'image' => 'required'
        ]);
            $bookUpdate = $request->all();
            $book->fill($bookUpdate)->save();
            Session::flash('flash_message', 'Books successfully Updated!');


            $activity_log_data = $this->userData($request);
            $activity_log_data['action'] = 'update book: ';
            
            Log::create($activity_log_data);

       return redirect('books');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id , Request $request)
    {
       Book::find($id)->delete();
        $activity_log_data = $this->userData($request);
        $activity_log_data['action'] = 'delete book: ';

        Log::create($activity_log_data);
       return redirect('books');
    }

    public function multiple_destroy(Request $request)
    {

       $ids = $request->input('multiple_id');
       $ids = explode(',',$ids);
       foreach ($ids as $id) {
            Book::find($id)->delete();
       }
        $activity_log_data = $this->userData($request);
        $activity_log_data['action'] = 'delete selected book: ';
 
        Log::create($activity_log_data);
       return redirect('books');
    }

    public function destroy_all(Request $request)
    {

        Book::truncate();
        $activity_log_data = $this->userData($request);
            $activity_log_data['action'] = 'delete all book: ';            
            Log::create($activity_log_data);
        return redirect('books');
    }


   
    public function userData(Request $request){
        $user_id = Auth::user()->id;
        $username = Auth::user()->username;
        $current_url = $request->path();
        $ip_address = $request->ip();
    

        $activity_log_data = array(
            'user_id'       => $user_id,
            'username'      => $username,
            'current_url'   => $current_url,
            'ip_address'    => $ip_address
            );

        return $activity_log_data;
    }

    /*
        show logs to user
    */
    public function logs()
    {
        $user_id = Auth::user()->id;
        $logs = Log::where('user_id', '>', "'".$user_id."'")->get();
        return view('books.logs',compact('logs'));
    }

}
