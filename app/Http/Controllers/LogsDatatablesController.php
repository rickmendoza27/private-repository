<?php

namespace App\Http\Controllers;
use App\Models\Log;
use Response;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


class LogsDatatablesController extends Controller
{
    
    public function logData()
    {
        return self::logsdatatables(Log::select('username','created_at','current_url','ip_address')->get());
    }

   public static function logsdatatables($data) {

    $query = array(
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data);
        return response()->json($query,200);
    }
}
