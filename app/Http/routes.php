<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', 'WelcomeController@index');

// Route::get('home', 'HomeController@index');

// Route::controllers([
//    'auth' => 'Auth\AuthController',
//    'password' => 'Auth\PasswordController',
// ]);

Route::group(['middleware' => 'log'], function () {

Route::get('books/logs', array('uses' => 'BookController@logs'));

Route::resource('users','RegisterUserController');
Route::post('books/bulkdelete','BookController@multiple_destroy');
Route::post('books/deleteall','BookController@destroy_all');
Route::post('books/{id}','BookController@destroy');
Route::resource('books','BookController');


Route::model( 'user' , 'App\Models\User' );


Route::get('/', function() {
  return View::make('login');
});

//POST route
Route::post('login', array('uses' => 'AccountController@login'));
Route::get('logout', array('uses' => 'AccountController@logout'));

Route::get('datatables', 'DatatablesController@anyData');
Route::get('logsdatatables', 'LogsDatatablesController@logData');

});