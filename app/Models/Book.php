<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    //
       protected $fillable=[
       	'id',
        'isbn',
        'title',
        'author',
        'publisher',
        'image'
    ];
}
